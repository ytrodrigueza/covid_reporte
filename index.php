<?php
require_once 'logica/Region.php';
require_once 'logica/Country.php';
require_once 'logica/Report.php';

$pid="";

if(isset($_GET["pid"])){
    
    $pid= base64_decode($_GET["pid"]);
}

?>

<!doctype html>
<html lang="es">
<head>
<!-- utf-8  para las tíldes en español -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- JS , incluye popper (para los tooltips)-->
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js">
	</script>

<!-- CSS -->
<link rel="stylesheet" href="css/estilos.css" />

<!--iconos font awesome-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />

<!--JQUERY libreria de JS-->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<!-- letra -->
<link
	href="https://fonts.googleapis.com/css2?family=Fredericka+the+Great&display=swap"
	rel="stylesheet">

<!-- Titulo pestaña con imágen -->
<title>Reporte covid</title>
<link rel="icon" type="image/png" href="img/logo_pesta.png" />
</head>

<body>


<?php 

if($pid!=""){
    
    include $pid;
}else{
    
    include "presentacion/consultar_pais.php";
}

?>


</body>
<footer>

	<div class="container">
		<div class="row mt-3">
			<div class="row">
				<div class="col text-center text-muted">
					<i class="fas fa-skull "></i> Y &copy; <?php echo date("Y") ?>	
				</div>
			</div>
		</div>
	</div>


</footer>
</html>