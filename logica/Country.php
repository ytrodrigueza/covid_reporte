<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CountryDAO.php";


class Country{
    private $id_country;
    private $name;
    private $region;
    private $conexion;
    private $countryDAO;
    
  
    /**
     * @return mixed
     */
    public function getId_country()
    {
        return $this->id_country;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    
    public function Country($id_country="", $name="",$region="") {
        $this -> id_country = $id_country;
        $this -> name = $name;
        $this -> region = $region;
        $this -> conexion = new Conexion();
        $this -> countryDAO = new CountryDAO($this -> id_country, $this -> name,$this -> region);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> regionDAO -> consultar());
        $this -> conexion -> cerrar();
        $this -> name=$this -> conexion -> extraer()[0];
    }
    
    public function consultar_todos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> countryDAO -> consultar_todos());
        $paises= array();
        while(($resultado=$this->conexion->extraer())!= null){
            
            $region= new Region($resultado[2]);
            $region->consultar();
            array_push($paises,new Country($resultado[0],$resultado[1],$region));
            
        }
        $this->conexion->cerrar();
        return $paises;
        
    }
    
    
    
}

