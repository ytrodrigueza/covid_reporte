<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/RegionDAO.php";


class Region{
    private $id_region;
    private $name;
    private $conexion;
    private $regionDAO;
  
    
    
    /**
     * @return string
     */
    public function getId_region()
    {
        return $this->id_region;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    
    public function Region($id_region="", $name="") {
        $this -> id_region = $id_region;
        $this -> name = $name;
        $this -> conexion = new Conexion();
        $this -> regionDAO = new RegionDAO($this -> id_region, $this -> name);
    }
    
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> regionDAO -> consultar());
        $this -> conexion -> cerrar();
      //  $datos = $this -> conexion -> extraer();
      //  $this -> name = $datos[0];
        $this -> name=$this -> conexion -> extraer()[0];
    }
    
    public function consultar_todos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> regionDAO -> consultar_todos());
        $regiones= array();
        while(($resultado=$this->conexion->extraer())!= null){
            array_push($regiones,new Region($resultado[0],$resultado[1]));
           
        }
        $this->conexion->cerrar();
        return $regiones;
       }
    
   
    
}

