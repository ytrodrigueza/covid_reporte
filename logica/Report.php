<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ReportDAO.php";


class Report{
    private $id_report;
    private $date;
    private $new_cases;
    private $cumulative_cases;
    private $new_deaths;
    private $cumulative_deaths;
    private $country;
    private $conexion;
    private $reportDAO;
    
    
    
    
    public function Report($id_report=0, $date="",$new_cases="",$cumulative_cases="", $new_deaths="",$cumulative_deaths="",$country="") {
        $this -> id_report = $id_report;
        $this -> date = $date;
        $this -> new_cases = $new_cases;
        $this -> cumulative_cases = $cumulative_cases;
        $this -> new_deaths = $new_deaths;
        $this -> cumulative_deaths = $cumulative_deaths;
        $this -> country = $country;
        $this -> conexion = new Conexion();
        $this -> reportDAO = new ReportDAO($this -> id_country, $this -> date,$this -> new_cases,$this -> cumulative_cases, $this -> new_deaths,$this -> cumulative_deaths,$this -> country);
    }
    
    public function buscar($pais){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> reportDAO -> buscar($pais));
        $reportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            array_push($reportes, $registro);
        }
        $this -> conexion -> cerrar();
        return  $reportes;
    }
    
    
    
}

