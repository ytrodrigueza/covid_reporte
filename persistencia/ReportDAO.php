<?php


class ReportDAO{
    private $id_report;
    private $date;
    private $new_cases;
    private $cumulative_cases;
    private $new_deaths;
    private $cumulative_deaths;
    private $country;

    
    
    
    
    public function Report($id_report, $date,$new_cases,$cumulative_cases, $new_deaths,$cumulative_deaths,$country) {
        $this -> id_report = $id_report;
        $this -> date = $date;
        $this -> new_cases = $new_cases;
        $this -> cumulative_cases = $cumulative_cases;
        $this -> new_deaths = $new_deaths;
        $this -> cumulative_deaths = $cumulative_deaths;
        $this -> country = $country;
    }
    
    
    public function buscar($pais){
        //    consultar el reporte que me estan pidiendo de acuerdo al pais, es necesario hacer inner joins
         
        return "select reg.name as Region, cou.id_country as Codigo_pais, cou.name as Pais, rep.cumulative_cases as Casos_acumulados, rep.cumulative_deaths as Muertes_acumuladas
                from region reg join country cou on (reg.id_region = cou.id_region_region)
                                join report rep on (cou.id_country = rep.id_country_country)
                where cou.id_country= '" . $pais . "'";
    }
        
    
    
    
    
    }