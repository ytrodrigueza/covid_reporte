<?php
include "presentacion/encabezado.php";
?>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Paises</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-4"></div>
						<div class="col-4 text-center">
							<div class="mb-3">
							<label class="form-label">Paises: </label>
								<select  id="pais">
								<?php 
								$pais = new Country();
								$paises = $pais->consultar_todos();
								foreach ($paises as $pais_actual){
								    echo "<option value='" . $pais_actual -> getId_country() . "'>" . $pais_actual -> getName() . "</option>";
								}
								
								?>
									
								</select>


							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div id="resultados"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>

$(document).ready(function() {
    
    $("#pais").change(function(){
    	var id_pais= $("#pais").val();
        var url="index_ajax.php?pid=<?php echo base64_encode("presentacion/consultar_pais_ajax.php")?>&pais="+ id_pais;
        $("#resultados").load(url);
        alert("Hizo click en el pais con valor: "+ id_pais);
        
    });
});
    
 



</script>