<?php

  $pais = $_GET["pais"];
  $report=new Report();
  $reports=$report->buscar($pais);
 
// aca se vera la tabla con los resultados esperados de acuerdo al filtro enviado que es el pais
?>

<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Región</th>
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Casos acumulados</th>
			<th>Muertes acumuladas</th>
			
		</tr>
	</thead>
	<tbody>
							<?php
							//pilas porque si llama a pais lo unico que va a salir es el pais, debo hacer consulta en cascada
							foreach ($reports as $actual_report) {
        echo "<tr>";
        echo "<td>" . $actual_report[0] . "</td>
        <td>" . $actual_report[1] . "</td>
        <td>" . $actual_report[2] . "</td>
        <td>" . $actual_report[3] . "</td>
        <td>" . $actual_report[4] . "</td>";

        echo "</tr>";
    }  
    ?>
						</tbody>
</table>




